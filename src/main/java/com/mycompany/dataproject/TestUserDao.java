/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dataproject;

import com.mycompany.dataproject.dao.UserDao;
import com.mycompany.dataproject.helper.DatabaseHelper;
import com.mycompany.dataproject.model.User;

/**
 *
 * @author Sabusi
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        //System.out.println(userDao.getAll());
        for(User u: userDao.getAll()){
            System.out.println(u);
        }
//        User user = userDao.get(2);
//        System.out.println(user);
        
        //User newUser = new User("user3", "password", 2, "F");
        //User insertedUser = userDao.save(newUser);
        //System.err.println(insertedUser);
        
//        user.setGender("F");
//        userDao.update(user);
//        User updateUser = userDao.get(user.getId());
//        System.out.println(updateUser);
//        
//        userDao.delete(user);
//        for(User u: userDao.getAll()){
//            System.out.println(u);
//        }
        
        
        for(User u: userDao.getAll(" user_name like 'u%' "," user_name asc, user_gender desc ")){
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
